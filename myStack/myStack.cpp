#include<iostream>
#include "myStack.h"
#include<stdlib.h>
using namespace std;
myStack::myStack()                                   //myStack constructor
{
}


myStack::~myStack()                                 //myStack destructor
{
}

int myStack::getData()                              //get any element you want to insert to myStack
{
	return data;
}

void myStack::setData(int data)                     //set the inserted data
{
	this->data = data;
}


int myStack::Empty(myStack *link)                              //tests if the stack is empty 
{
	if (link == NULL)
		return (1);                                             //returns 1 if stack is empty
	else
		return 0;                                               //returns 0 if stack is not empty
} 

void myStack::pop(myStack *link,int *item)                     //displays the stack, traversing pointer *item
{ 
	{
		myStack *p;                                            //traversing pointer
		p = link; 
		if (Empty(p))                                         //tests if stack is empty first
		{
			printf("Error The stack is empty. \n");
			data = '\0';
		}
		else                                                 //proceeds with displaying the elements in the stack if the stack is not empty
		{ 
			*item = p->data;
			link = p->link;
			free(p);
		}
	}
}

void myStack::push(myStack *link,int item)                  //insert elements in the stack
{
	myStack *p;                                            //traversing pointer
	if (Empty(link))                                       //tests if stack is empty
	{
		p = (myStack *)malloc(sizeof(myStack));           //allocate memory if stack is emepty
		if (p != NULL) 
		{
			p->data = item;
			p->link = NULL;
			link = p;
		}
	}
	else
	{
		p = (myStack *)malloc(sizeof(myStack));           //insert elements in the stack
		if (p != NULL)
		{
			p->data = item;
			p->link = link;
			link = p;
		}
	}
}
