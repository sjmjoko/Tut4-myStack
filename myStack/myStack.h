#pragma once
#ifndef myStack_H
#define myStack_H
class myStack
{
private:
	int data;                                               //data field of myStack
	myStack *link;                                         //link field of myStack
public:   
	myStack();                                            //constructor
	~myStack();                                          //destructor

	void setData(int info);                             //get the data from the user
	int getData();                                     //get data from the user
	       
	//function declarations
	void push(myStack *link,int data);                //inserting elements from the usr
	void pop(myStack *link,int *item);               //displaying the elements in the stack
	void peek();
	int isFull();                                   //tests if the stack is full
	int Empty(myStack *link);                      //tests if the stack is empty
};
#endif

